/*
** EPITECH PROJECT, 2023
** my_cat
** File description:
** cat
*/

// clang-format off

#ifndef CAT_H_
    #define CAT_H_

    #ifndef MC_BUFFER_SIZE
        #define MC_BUFFER_SIZE 2048
    #endif

    #define likely(x) __builtin_expect((x), 1)
    #define unlikely(x) __builtin_expect((x), 0)

#endif /* !CAT_H_ */

// clang-format n
