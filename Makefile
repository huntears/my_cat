##
## EPITECH PROJECT, 2023
## my_cat
## File description:
## Really simple Makefile cause augh
##

CFLAGS	:=	-Wall -Wextra -O3 -flto -Weverything -Wno-unsafe-buffer-usage -D_FORTIFY_SOURCE=2

LDFLAGS	:=	-flto -s

CC		:=	clang

all: my_cat

my_cat: cat.o
	${CC} ${LDFLAGS} cat.o -o my_cat

clean:
	rm -f *.o

fclean: clean
	rm -f my_cat

re:
	${MAKE} fclean
	${MAKE} all
